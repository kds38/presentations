+++
title = "My presentation"
outputs = ["Reveal"]
+++

# TCA&S' Drupal 8 Distribution
<hr/>

### Or… how we're minimizing support, timeline, and development costs

<hr/>

## This presentation:
http://bit.ly/tcas-drupal8-distro

---

## Your presenter 
<hr/>

## Kyle Skrinak, I.T. Manager 
### Trinity College of Arts & Sciences   
Web Support (now part of OIT), Duke University
https://people.duke.edu/~kds38

---


## Meet the team 
<hr/> 

* Andy Smith, *Web Dev & Design*
  * *Co-presenting*
* Alex Verhoeven, *Web Dev & Design*
  * *Co-presenting*
* Gabe Fahl, *PHP Programmer*
* John Herr, *PHP Programmer*
* David Palmer, *DevOps*

{{% note %}}
Andy and Alex,

Please introduce yourselves. Job title, what you do, how long you've been at Duke.
{{% /note %}}


----


## Who We Serve?

<hr/>

**Trinity College of Arts & Sciences   
departments, administrative, labs, etc.**

* 120 active websites
* 100+ legacy websites  
and web applications 

<hr/>

**SLAs and MoUs, *including***

  * divinity.duke.edu
  * dibs.duke.edu
  * sanford.duke.edu

---

## Requirements

<hr/>

* Consistency of branding and functionality across all of our websites
* Flexible, consistent, and centrally-managed editorial experience
* Weekly collaboration with our Trinity Communication team
* Rely on Open Source community, and not custom module development
* Decreased support and discovery time for a small team
* Agility with code updates, website maintenance, and deployment
* Minimize cost for external tools
* Managing D7 - D8 migrations via core 

---

## Our D8 Implementation

<hr/>

## Front-end and Editorial

* Managing our branding with a Bootstrap-based parent theme and subthemes
* Using "paragraphs" for flexible yet managed page content
* Extensive user documentation and training
  * Inline help within the content editor experience
  * https://admin.trinity.duke.edu/technology/drupal-website-editing 

---


## Our D8 Implementation

<hr/>

## Back-end

* Continued Drupal multi-site for hosting
  * Currently, David is exploring replicating our hosting on to OKD.
* How we stand up new websites from our distribution
* Managing consistent configuration management across all websites
* Using GitLab's CI/CD for a dashboard-like pushbutton deployment
  * Next slide >

---

## GitLab buttons

<font size="6">

| Site tasks                  | Pushbuttons         | Revert      | Migrate         |
|-----------------------------|---------------------|-------------|-----------------|
| Enable maintenance mode     | Production backsync | Restore DB  | Staging to Prod |
| DB Backup                   | Git pull \<branch\> | Revert code |                 |
| Git Reset --hard \<branch\> |                     |             |                 |
| Drush entity update         |                     |             |                 |
| Drush clear caches          |                     |             |                 |
| Apache OpCache clear        |                     |             |                 |
| Drush config export         |                     |             |                 |
| Drush config import         |                     |             |                 |
| Fix permissions             |                     |             |                 |
| Disable maintenance mode    |                     |             |                 |
| Backsync DB from prod       |                     |             |                 | 

</font>

{{% note %}} 
Workflows and environments

- met with the team to define environments and workflows

- agreed on three environments: development, staging, and production

- two workflows: which determine how to move data, code and configuration for new sites and existing sites

Site level operations

- helpful in the process of creating workflows and useful for performing site level tasks without shell acfcess

New sites are a one way workflow

When working with new sites our migrations enabled us to easily move databases, code and configuration from development to staging and staging to production

Maintaining existing sites is a circular workflow

When modifying existing sites, developers have the ability of backsync’ing the database from production to development 
Then when deploying changes from development to staging, the production database is backsync’ed to the staging server and the code and config changes are pulled to the staging server. This provides a true test of what will happen when the changes are deployed to production

When changes are deployed to production, only code and configuration are pulled and applied to the production environment. 

{{% /note %}}

---

## Ongoing approach

<hr/>

* Continue to enhance our content editorial experience
* Weekly meetings with the TCA&S Comm team
* Tamp down scope creep as we scale
* Synchronize core features and display across all websites
* Extend our CI/CD to WordPress and other servers
* In process: 
  * Default site content
  * OKD for hosting
  * Prepare for D9

--- 

## Q & A 

<hr/>

> *thank you for watching!*
